/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rafarods.traynoafk;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Main program for TrayNoAFK
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class AppMain {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(AppMain.class.getName());
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("DefaultBundle");
    private static final String APP_NAME = RESOURCE_BUNDLE.getString("app.name");

    /**
     * Returns the Java version as an int value.
     *
     * @return the Java version as an int value (8, 9, etc.)
     */
    private static boolean isJavaVersionOk() {
        // Java 8 >= 52.0
        return Double.parseDouble(System.getProperty("java.class.version")) >= 52;
    }

    /**
     * @return True or false when instance is locked.
     */
    private static boolean lockInstance() {
        final File file = new File(System.getProperty("java.io.tmpdir"), RESOURCE_BUNDLE.getString("app.name.id").concat(".lock"));
        LOGGER.log(Level.INFO, "Lock file in {0}", file.getAbsolutePath());
        try {
            final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            final FileLock fileLock = randomAccessFile.getChannel().tryLock();
            if (fileLock != null) {
                randomAccessFile.writeChars(AppMain.class.getSimpleName() + " " + LocalDateTime.now());
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    @Override
                    public void run() {
                        try {
                            fileLock.release();
                            randomAccessFile.close();
                        } catch (final IOException ex) {
                            LOGGER.info(ex.getMessage());
                        } finally {
                            file.deleteOnExit();
                        }
                    }
                });
                return true;
            }
        } catch (final IOException ex) {
            LOGGER.info(ex.getMessage());
        }
        return false;
    }

    /**
     * @param args Arguments.
     */
    public static void main(final String[] args) {
        final SplashScreen splash = SplashScreen.getSplashScreen();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (final InterruptedException ex) {
            LOGGER.severe(ex.getMessage());
        }
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        LOGGER.log(Level.INFO, "TimeZone is {0}", TimeZone.getDefault().getID());
        LOGGER.log(Level.INFO, "Locale is {0}", Locale.getDefault());
        if (!isJavaVersionOk()) {
            JOptionPane.showMessageDialog(null, RESOURCE_BUNDLE.getString("app.jvm.version.message"), APP_NAME, JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        if (!SystemTray.isSupported()) {
            JOptionPane.showMessageDialog(null, RESOURCE_BUNDLE.getString("app.system-tray.message"), APP_NAME, JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        if (!lockInstance()) {
            LOGGER.info("Application instance locked!");
            JOptionPane.showMessageDialog(null, RESOURCE_BUNDLE.getString("app.instance.locked.message"), APP_NAME, JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SystemTray.getSystemTray().add(new AppTrayIcon());
        } catch (final AWTException | ClassNotFoundException | InstantiationException | IllegalAccessException
                       | UnsupportedLookAndFeelException ex) {
            LOGGER.severe(ex.getMessage());
        } finally {
            if (splash != null) {
                LOGGER.log(Level.INFO, "Splash screen image: {0}", splash.getImageURL().getFile());
                splash.close();
            }
        }
    }
}
