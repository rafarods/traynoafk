/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rafarods.traynoafk;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.text.MessageFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 *
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class TimerDialog extends JDialog {
    private static final Logger LOGGER = Logger.getLogger(TimerDialog.class.getName());
    public static final String TIME_FORMAT = "HH:mm:ss";
    private final JTextField txtTimer = new JTextField();
    private final transient ResourceBundle resourceBundle = ResourceBundle.getBundle("DefaultBundle");

    /**
     * Creates a dialog with an empty title and the specified modality and Frame as its owner. If owner is null, a shared,
     * hidden frame will be set as the owner of the dialog.
     * This constructor sets the component's locale property to the value returned by JComponent. getDefaultLocale.
     * NOTE: This constructor does not allow you to create an unowned JDialog. To create an unowned JDialog you must use
     * either the JDialog(Window) or JDialog(Dialog) constructor with an argument of null.
     *
     * @param parent The Frame from which the dialog is displayed.
     * @param modal Specifies whether dialog blocks user input to others.
     */
    public TimerDialog(final Frame parent, final boolean modal) {
        super(parent, modal);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setAlwaysOnTop(true);
        this.setIconImage(new ImageIcon(Objects.requireNonNull(getClass().getResource("/icon.png"))).getImage());
        this.txtTimer.setHorizontalAlignment(SwingConstants.CENTER);
        final JLabel lblWiggleTime = new JLabel();
        lblWiggleTime.setLabelFor(txtTimer);
        lblWiggleTime.setText(this.resourceBundle.getString("app.idle-time.form.label"));
        final JLabel lblWiggleTimeFormat = new JLabel();
        lblWiggleTimeFormat.setHorizontalAlignment(SwingConstants.CENTER);
        lblWiggleTimeFormat.setLabelFor(this.txtTimer);
        lblWiggleTimeFormat.setText(TIME_FORMAT);
        final JButton btnSetTimer = new JButton();
        btnSetTimer.setName("btnSetTimer");
        btnSetTimer.setText(this.resourceBundle.getString("app.idle-time.form.set"));
        btnSetTimer.addActionListener(this::btnSetTimerActionPerformed);
        final GroupLayout layout = new GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(lblWiggleTime)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(
                                        GroupLayout.Alignment.LEADING, false)
                                        .addComponent(lblWiggleTimeFormat,
                                                GroupLayout.DEFAULT_SIZE,
                                                GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)
                                        .addComponent(this.txtTimer,
                                                GroupLayout.PREFERRED_SIZE,
                                                65,
                                                GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
                                        GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE)
                                .addComponent(btnSetTimer,
                                        GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                                        GroupLayout.PREFERRED_SIZE)
                                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(
                                        GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblWiggleTime)
                                        .addComponent(this.txtTimer,
                                                GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE,
                                                GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnSetTimer))
                                .addGap(1, 1, 1)
                                .addComponent(lblWiggleTimeFormat)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE)));
        pack();
    }

    private void btnSetTimerActionPerformed(final ActionEvent evt) {
        if (this.txtTimer.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Time is required",
                    resourceBundle.getString("app.name"), JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                final LocalTime time = LocalTime.parse(this.txtTimer.getText(),
                        DateTimeFormatter.ofPattern(TIME_FORMAT));
                if (time.equals(LocalTime.of(0, 0, 0))) {
                    throw new DateTimeParseException("Invalid time " + this.txtTimer.getText(), TIME_FORMAT, 0);
                }
                this.dispose();
                LOGGER.log(Level.INFO, "Set waiting time to {0}", this.txtTimer.getText());
            } catch (final DateTimeParseException ex) {
                LOGGER.info(ex.getMessage());
                JOptionPane.showMessageDialog(this,
                        MessageFormat.format(resourceBundle.getString("app.idle-time.invalid"),
                                this.txtTimer.getText()),
                        resourceBundle.getString("app.name"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public JTextField getTxtTimer() {
        return this.txtTimer;
    }
}
