/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rafarods.traynoafk;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class WaitingThread extends Thread {

    private static final Logger LOGGER = Logger.getLogger(WaitingThread.class.getName());
    private static final short OFF_X = 1;
    private static final short OFF_Y = 1;
    private final WaitTime waitTime = new WaitTime();
    private Robot robot;

    public WaitingThread() {
        try {
            this.robot = new Robot();
        } catch (final AWTException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                final Point point = MouseInfo.getPointerInfo().getLocation();
                int x = (int) point.getX();
                int y = (int) point.getY();
                robot.mouseMove(x + OFF_X, y + OFF_Y);
                robot.mouseMove(x - OFF_X, y - OFF_Y);
                robot.mouseMove(x, y);
                LOGGER.log(Level.FINER, "{0}", "Wating time: " + this.waitTime.getTime() + " " + "Wiggle mouse: " + x + "-" + y);
                Thread.sleep(this.waitTime.getTimeInMilliseconds());
            } catch (final IllegalArgumentException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            } catch (final InterruptedException ex) {
                while (!Thread.currentThread().isAlive()) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public synchronized WaitTime getWaitTime() {
        return this.waitTime;
    }
}
