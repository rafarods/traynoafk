/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rafarods.traynoafk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class AppTrayIcon extends TrayIcon {
    private static final Logger LOGGER = Logger.getLogger(AppTrayIcon.class.getName());
    private final PopupMenu trayPopupMenu = new PopupMenu();
    private final WaitingThread waitingThread = new WaitingThread();
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("DefaultBundle");
    private final String appName = resourceBundle.getString("app.name");
    private final MenuItem menuIdleTime = new MenuItem();
    private final MenuItem menuClose = new MenuItem();
    private final Menu menuLang = new Menu();
    private final CheckboxMenuItem chkMenuEng = new CheckboxMenuItem();
    private final CheckboxMenuItem chkMenuSpa = new CheckboxMenuItem();
    private TimerDialog formTimerDialog = new TimerDialog(null, true);

    /**
     * Add tray icon settings.
     */
    public AppTrayIcon() {
        super(Toolkit.getDefaultToolkit().getImage(AppTrayIcon.class.getResource("/icon.png")));
        this.setToolTip(this.appName);
        this.setPopupMenu(this.trayPopupMenu);
        this.setImageAutoSize(true);
        this.addMenuIdleTimeAction();
        this.addMenuLanguage();
        this.trayPopupMenu.addSeparator();
        this.addMenuCloseAction();
        this.waitingThread.start();
    }

    /**
     * Add set idle time action
     */
    private void addMenuIdleTimeAction() {
        this.menuIdleTime.setLabel(resourceBundle.getString("app.idle-time.label"));
        this.menuIdleTime.addActionListener((ActionEvent e) -> {
            this.formTimerDialog.getTxtTimer().setText(this.waitingThread.getWaitTime().getTime().format(DateTimeFormatter.ofPattern(TimerDialog.TIME_FORMAT)));
            this.formTimerDialog.setTitle(this.appName);
            this.formTimerDialog.setAlwaysOnTop(true);
            this.formTimerDialog.setLocationRelativeTo(null);
            this.formTimerDialog.pack();
            this.formTimerDialog.setVisible(true);
            if (!this.formTimerDialog.getTxtTimer().getText().isEmpty()) {
                this.waitingThread.getWaitTime().setTimer(this.formTimerDialog.getTxtTimer().getText());
            }
            this.waitingThread.interrupt();
        });
        this.trayPopupMenu.add(this.menuIdleTime);
    }

    private void addMenuLanguage() {
        this.menuLang.setLabel(resourceBundle.getString("app.language"));
        this.chkMenuEng.setLabel(resourceBundle.getString("app.language.english"));
        this.chkMenuSpa.setLabel(resourceBundle.getString("app.language.spanish"));
        final boolean isLangEng = Locale.getDefault().getLanguage().equals(Locale.ENGLISH.getLanguage());
        this.chkMenuEng.setState(isLangEng);
        // Toggle Spanish=Off and English=On
        this.chkMenuEng.addItemListener((ItemEvent e) -> {
            ((CheckboxMenuItem) e.getSource()).setState(true);
            this.chkMenuSpa.setState(false);
            if (!Locale.getDefault().equals(Locale.ENGLISH)) {
                Locale.setDefault(Locale.ENGLISH);
                this.refreshTextUI();
            }
        });
        this.chkMenuSpa.setState(!isLangEng);
        // Toggle Spanish=On and English=Off
        this.chkMenuSpa.addItemListener((ItemEvent e) -> {
            ((CheckboxMenuItem) e.getSource()).setState(true);
            this.chkMenuEng.setState(false);
            final Locale locSpa = new Locale("es");
            if (!Locale.getDefault().equals(locSpa)) {
                Locale.setDefault(locSpa);
                this.refreshTextUI();
            }
        });
        menuLang.add(this.chkMenuEng);
        menuLang.add(this.chkMenuSpa);
        this.trayPopupMenu.add(menuLang);
    }

    /**
     * Add close action menu
     */
    private void addMenuCloseAction() {
        this.menuClose.setLabel(resourceBundle.getString("app.exit.label"));
        this.menuClose.addActionListener((ActionEvent e) -> {
            final int result = JOptionPane.showConfirmDialog(null, resourceBundle.getString("app.exit.message"), this.appName, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                LOGGER.log(Level.INFO, "Doing the action: {0}", this.menuClose.getLabel());
                System.exit(0);
            }
        });
        this.trayPopupMenu.add(this.menuClose);
    }

    private void refreshTextUI() {
        this.resourceBundle = ResourceBundle.getBundle("DefaultBundle");
        if (this.formTimerDialog.isVisible()) {
            this.formTimerDialog.dispose();
        }
        this.formTimerDialog = new TimerDialog(null, true);
        this.menuClose.setLabel(resourceBundle.getString("app.exit.label"));
        this.menuLang.setLabel(resourceBundle.getString("app.language"));
        this.chkMenuEng.setLabel(resourceBundle.getString("app.language.english"));
        this.chkMenuSpa.setLabel(resourceBundle.getString("app.language.spanish"));
        this.menuIdleTime.setLabel(resourceBundle.getString("app.idle-time.label"));
    }
}
