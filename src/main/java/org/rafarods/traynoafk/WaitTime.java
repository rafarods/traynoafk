/*
 * Copyright (C) 2023 Rafael Rodriguez <rafarods@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rafarods.traynoafk;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Rafael Rodriguez <rafarods@outlook.com>
 */
public class WaitTime {
    private static final Logger LOGGER = Logger.getLogger(WaitTime.class.getName());

    /**
     * Set timer
     */
    private LocalTime timer = LocalTime.of(0, 2, 0);

    /**
     *
     */
    public synchronized LocalTime getTime() {
        return this.timer;
    }

    /**
     *
     */
    public synchronized int getTimeInMilliseconds() {
        return this.getTime().get(ChronoField.MILLI_OF_DAY);
    }

    /**
     * Set new time.
     *
     * @param timer Time to set
     */
    public synchronized void setTimer(final LocalTime timer) {
        this.timer = timer;
        LOGGER.log(Level.FINE, "Set timer {0}", this.timer);
    }

    public synchronized void setTimer(final String timer) {
        this.setTimer(LocalTime.parse(timer, DateTimeFormatter.ofPattern(TimerDialog.TIME_FORMAT)));
    }
}
